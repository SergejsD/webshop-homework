Application Development Fundamentals Homework  
====
Status: [![pipeline status](https://gitlab.com/SergejsD/webshop-homework/badges/master/pipeline.svg)](https://gitlab.com/SergejsD/webshop-homework/commits/master) [![coverage report](https://gitlab.com/SergejsD/webshop-homework/badges/master/coverage.svg)](https://gitlab.com/SergejsD/webshop-homework/commits/master)  

Asigment  
===
Homework is a practical part of the training. It's mission is to consolidate received knowledge from online courses in practice building the application. You are free to work with your own idea for the application or can pick one from the topics below:  

- Movie Rating System  
- Song Recommendation System  
- Online Shopping website  
- Library Management System  
- Social Network for a College/University  

Assignment Requirements
========================

-  Create the project using Maven/Gradle
-  create a branch from InnerSource repository and commit your App into that branch. *Done entirely on GitLab*
-  Implement the main logic of your selected topics with all the needed services, UIs, DTO, etc.
-  Your app should use Spring framework
-  All services must have 70% to 80% test coverage
-  Your app should have both Unit and Integration tests
-  you should create Jenkins jobs to build and test the application. *Done entirely using GitLab Ci*
-  All code must comply with the Java naming conventions and best practices(use SonalLint/SonaQube ) *Used InteliJ SonarLint plugin*
-  At the presentation session you should be able to demo at least 2 working functions on your app
-  You should provide a presentation showing a Sonar report on the test coverage and code quality in your app and explaining the idea you&#39;ve picked and implemented
-  You should have at least 3 commits per person on InnerSource repository for  your app.

SonarLintReport  
===
TBD inser screenshot here

App idea
===
Simple web shop inventory management system which provides all crud operations to manage inventory items
