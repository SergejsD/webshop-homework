package com.sergejs.webshop.controller;

import com.sergejs.webshop.dto.ProductListingDto;
import com.sergejs.webshop.service.WebShopService;
import com.sergejs.webshop.utils.ProductType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Slf4j
@Controller
@RequiredArgsConstructor
public class WebShopController {

    private static final String SELECTED_TYPE_ATR = "selectedType";
    private static final String PRODUCTS_ATR = "products";
    private static final String PRODUCT_ATR = "product";
    private static final String PRODUCTS_TYPES_ATR = "productTypes";
    private final WebShopService webShopService;

    @GetMapping("/")
    public String getAllProductList(Model model) {
        model.addAttribute(SELECTED_TYPE_ATR, null);
        model.addAttribute(PRODUCTS_TYPES_ATR, ProductType.values());
        model.addAttribute(PRODUCTS_ATR, webShopService.retrieveAllProducts().getPayload());
        return "main";
    }

    @GetMapping("/products/{type}")
    public String getProductListByType(@PathVariable(required = false) String type,
                                       Model model) {
        model.addAttribute(SELECTED_TYPE_ATR, type.toUpperCase());
        model.addAttribute(PRODUCTS_TYPES_ATR, ProductType.values());
        model.addAttribute(PRODUCTS_ATR, webShopService.retrieveProductsByType(type).getPayload());
        return "main";
    }

    @GetMapping("/product/{id}")
    public String getProductPage(@PathVariable long id, Model model) {
        model.addAttribute(PRODUCT_ATR, webShopService.getProductInfo(id).getPayload());
        return "productPage";
    }

    @PostMapping("/product/{id}/delete")
    public String deleteProduct(Model model, @PathVariable long id) {
        webShopService.deleteProduct(id);
        return "redirect:/";
    }

    @GetMapping("/product/create")
    public String provideCreatePage(Model model) {
        model.addAttribute(PRODUCT_ATR, null);
        model.addAttribute(PRODUCTS_TYPES_ATR, ProductType.values());
        return "productEdit";
    }

    @GetMapping("/product/{id}/edit")
    public String getProductEditPage(@PathVariable long id, Model model) {
        model.addAttribute(PRODUCT_ATR, webShopService.getProductInfo(id).getPayload());
        model.addAttribute(PRODUCTS_TYPES_ATR, ProductType.values());
        return "productEdit";
    }

    @PostMapping(value = {"/product/{id}", "/product/"}, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String updateProduct(Model model,
                                @PathVariable(required = false) Long id,
                                ProductListingDto dto) {
        if (id != null) {
            dto.setId(id);
        }
        model.addAttribute(PRODUCT_ATR, webShopService.updateProduct(dto).getPayload());
        return "productPage";
    }

}
