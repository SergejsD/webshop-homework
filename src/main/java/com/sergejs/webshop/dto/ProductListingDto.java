package com.sergejs.webshop.dto;

import lombok.Data;

@Data
public class ProductListingDto {

    private Long id;
    private String name;
    private String description;
    private double price;
    private int amount;
    private String type;

}