package com.sergejs.webshop.dto;

import lombok.Data;

@Data
public class SingleResponseDto<T> {
    private T payload;

    public SingleResponseDto(T payload) {
        this.payload = payload;
    }

}