package com.sergejs.webshop.dto;

import lombok.Data;

import java.util.List;

@Data
public class CollectionResponseDto<T> {
    private List<T> payload;

    public CollectionResponseDto(List<T> payload) {
        this.payload = payload;
    }

}