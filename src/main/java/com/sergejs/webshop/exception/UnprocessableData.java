package com.sergejs.webshop.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Entered data in one of the fields wasn't valid")
public class UnprocessableData extends RuntimeException {

}
