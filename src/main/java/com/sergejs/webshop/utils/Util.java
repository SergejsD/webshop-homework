package com.sergejs.webshop.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Util {

    public static long unwrapLongValue(Long input) {
        return input == null ? 0L : input;
    }
}
