package com.sergejs.webshop.utils;

public enum ProductType {

    LAPTOP, DESKTOP, HOUSEHOLD, SMARTPHONE
}
