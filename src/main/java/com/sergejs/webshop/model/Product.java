package com.sergejs.webshop.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@Entity
@NoArgsConstructor
@ToString
public class Product {

    @Id
    @GeneratedValue
    private Long productId;

    private String name;
    private double price;
    private int amount;
    private String type;
    @Column(length = 1000)
    private String description;

    public Product(String name, double price, int amount, String type, String description) {
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.type = type;
        this.description = description;
    }


}