package com.sergejs.webshop.service;

import com.sergejs.webshop.dto.CollectionResponseDto;
import com.sergejs.webshop.dto.ProductListingDto;
import com.sergejs.webshop.dto.SingleResponseDto;
import com.sergejs.webshop.exception.DataNotFound;
import com.sergejs.webshop.mapper.ProductListingMapper;
import com.sergejs.webshop.model.Product;
import com.sergejs.webshop.model.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class WebShopService {

    private final ProductRepository productRepository;
    private final ProductListingMapper productListingMapper;
    private final WebShopServiceHelper webShopServiceHelper;

    public CollectionResponseDto<ProductListingDto> retrieveAllProducts() {
        List<Product> products = productRepository.findAll();

        return new CollectionResponseDto<>(products.stream()
                .map(productListingMapper::toDto)
                .collect(Collectors.toList()));
    }

    public CollectionResponseDto<ProductListingDto> retrieveProductsByType(String type) {

        List<Product> products = productRepository.findByType(type.toUpperCase());
        return new CollectionResponseDto<>(products.stream()
                .map(productListingMapper::toDto)
                .collect(Collectors.toList()));
    }

    public SingleResponseDto<ProductListingDto> getProductInfo(long id) {
        Product product = Optional.ofNullable(productRepository.findById(id))
                .orElseThrow(DataNotFound::new);
        return new SingleResponseDto<>(productListingMapper.toFullDto(product));
    }

    public SingleResponseDto<ProductListingDto> updateProduct(ProductListingDto dto) {
        webShopServiceHelper.validateUpdateData(dto);
        Product entity = productListingMapper.toEntity(dto);
        return new SingleResponseDto<>(productListingMapper.toFullDto(productRepository.save(entity)));
    }

    public void deleteProduct(long id) {
        productRepository.deleteById(id);
    }
}
