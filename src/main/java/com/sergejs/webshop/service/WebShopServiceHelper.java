package com.sergejs.webshop.service;

import com.sergejs.webshop.dto.ProductListingDto;
import com.sergejs.webshop.exception.UnprocessableData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class WebShopServiceHelper {

    public void validateUpdateData(ProductListingDto product) {
        if (product.getPrice() < 0) {
            log.error("product price can`t be negative Product:{}", product.toString());
            throw new UnprocessableData();
        }
        if (product.getAmount() < 0) {
            log.error("product can't have negative amount, Product:{}", product.toString());
            throw new UnprocessableData();
        }
        if (product.getName() != null && product.getName().isEmpty()) {
            log.warn("product name shouldn't`t be empty");
        }
    }
}
