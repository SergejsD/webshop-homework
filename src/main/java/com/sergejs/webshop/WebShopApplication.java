package com.sergejs.webshop;

import com.sergejs.webshop.model.Product;
import com.sergejs.webshop.model.ProductRepository;
import com.sergejs.webshop.utils.ProductType;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableAutoConfiguration
public class WebShopApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebShopApplication.class, args);
    }

    @Bean
    public CommandLineRunner loadData(ProductRepository repo) {
        return args -> {
            repo.save(new Product("Iphone X", 800.00, 1, ProductType.SMARTPHONE.toString(),
                    "iPhone X is a new smartphone from Apple that the company announced on September 12, 2017. The iPhone X gets its name from being a limited-edition \"exclusive\" model for 2017 as well as from its debut coming on the tenth anniversary of the original Apple iPhone release."));
            repo.save(new Product("Dell-5520", 2200.00, 3, ProductType.LAPTOP.toString(),
                    "On the world’s thinnest and lightest mobile workstation, performance has never looked so good. The compact design fits a 15.6-inch display into a 14-inch chassis, with an InfinityEdge display for virtually borderless viewing. Superior build quality and the carbon fiber palm rest deliver strength and durability." +
                            "\n In honor of our partners and customers, we celebrate 20 years of amazing work with the Precision 5520 Anniversary Edition. The world’s thinnest, lightest and smallest workstation is available for a limited time in an exclusive brushed Abyss Anodized exterior design, featuring exclusive wallpapers and premium packaging."));
            repo.save(new Product("Acer-E5-G375", 300.00, 3, ProductType.LAPTOP.toString(),
                    "Aspire E Series laptops are great choices for everyday users, with lots of appealing options and an attractive design that exceed expectations. With many enhanced components, color choices, and a textured metallic finish, the Aspire E makes everyday better." +
                            "\n "));
            repo.save(new Product("Best gaming station", 9000000.01, 3, ProductType.DESKTOP.toString(),
                    "TBD"));
            repo.save(new Product("Light Bulb", 1.0, 1000, ProductType.HOUSEHOLD.toString(),
                    "just best thing money can buy you, usually lights up when someone has great ideas"));

        };
    }

}