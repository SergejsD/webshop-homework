package com.sergejs.webshop.mapper;

import com.sergejs.webshop.dto.ProductListingDto;
import com.sergejs.webshop.model.Product;
import com.sergejs.webshop.utils.Util;
import org.springframework.stereotype.Component;

@Component
public class ProductListingMapper {

    public ProductListingDto toDto(Product product) {
        ProductListingDto dto = commonFieldMapper(product);
        dto.setDescription(product.getDescription() == null ? null : getShortDescription(product.getDescription()));
        return dto;
    }

    public ProductListingDto toFullDto(Product product) {
        ProductListingDto dto = commonFieldMapper(product);
        dto.setDescription(product.getDescription());
        return dto;
    }

    private ProductListingDto commonFieldMapper(Product product) {
        ProductListingDto dto = new ProductListingDto();
        dto.setId(Util.unwrapLongValue(product.getProductId()));
        dto.setName(product.getName());
        dto.setAmount(product.getAmount());
        dto.setPrice(product.getPrice());
        dto.setType(product.getType());
        return dto;
    }

    private String getShortDescription(String input) {
        return input.length() < 50 ? input : input.substring(0, 50) + "...";
    }

    public Product toEntity(ProductListingDto dto) {
        Product entity = new Product();
        entity.setProductId(dto.getId());
        entity.setName(dto.getName());
        entity.setPrice(dto.getPrice());
        entity.setAmount(dto.getAmount());
        entity.setDescription(dto.getDescription());
        entity.setType(dto.getType());
        return entity;
    }
}
