package com.sergejs.webshop.utils;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UtilTest {

    @Test
    public void stage01_nullInput() {
        Long result = Util.unwrapLongValue(null);
        assertNotNull(result);
        assertEquals(0L, result.longValue());
    }

    @Test
    public void stage02_normalInput() {
        Long result = Util.unwrapLongValue(123L);
        assertNotNull(result);
        assertEquals(123L, result.longValue());
    }

}