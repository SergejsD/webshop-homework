package com.sergejs.webshop.controller;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WebShopControllerTest {

    private static final String URL = "http://localhost:";
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void stage01_getAllProductsList_ok() {
        assertTrue(this.restTemplate.getForObject(URL + port + "/",
                String.class).contains("Iphone X"));
        assertTrue(this.restTemplate.getForObject(URL + port + "/",
                String.class).contains("Dell-5520"));
    }

    @Test
    public void stage02_getByType_ok() {
        assertTrue(this.restTemplate.getForObject(URL + port + "/products/laptop",
                String.class).contains("Dell-5520"));
        assertTrue(this.restTemplate.getForObject(URL + port + "/products/laptop",
                String.class).contains("Acer-E5-G375"));
        assertFalse(this.restTemplate.getForObject(URL + port + "/products/laptop",
                String.class).contains("Iphone X"));

    }

    /**
     * creates product with id 6 in db
     */
    @Test
    public void stage03_Create() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("name", "productName");
        map.add("price", "123");
        map.add("amount", "321");
        map.add("description", "it is description");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        ResponseEntity<String> response = this.restTemplate.postForEntity(URL + port + "/product/", request, String.class);
        System.out.println(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertTrue(response.getBody().contains("productName"));
        assertTrue(response.getBody().contains("123"));
        assertTrue(response.getBody().contains("321"));
        assertTrue(response.getBody().contains("it is description"));
    }

    @Test
    public void stage04_Read() {
        ResponseEntity<String> response = this.restTemplate.getForEntity(URL + port + "product/6", String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertTrue(response.getBody().contains("productName"));
        assertTrue(response.getBody().contains("123"));
        assertTrue(response.getBody().contains("321"));
        assertTrue(response.getBody().contains("it is description"));
    }

    @Test
    public void stage05_Update() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("name", "newProductName");
        map.add("price", "234");
        map.add("amount", "432");
        map.add("description", "it is new description");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        ResponseEntity<String> response = this.restTemplate.postForEntity(URL + port + "/product/6", request, String.class);
        System.out.println(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertTrue(response.getBody().contains("newProductName"));
        assertTrue(response.getBody().contains("234"));
        assertTrue(response.getBody().contains("432"));
        assertTrue(response.getBody().contains("it is new description"));
    }

    @Test
    public void stage06_ReadUpdated() {
        ResponseEntity<String> response = this.restTemplate.getForEntity(URL + port + "product/6", String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertTrue(response.getBody().contains("newProductName"));
        assertTrue(response.getBody().contains("234"));
        assertTrue(response.getBody().contains("432"));
        assertTrue(response.getBody().contains("it is new description"));
    }

    @Test
    public void stage07_Delete() {
        ResponseEntity<String> response = this.restTemplate
                .postForEntity(URL + port + "/product/6/delete", null, String.class);
        assertNotNull(response);
        assertEquals(HttpStatus.SEE_OTHER, response.getStatusCode());
    }

    @Test
    public void stage08_ReadDeleted() {
        ResponseEntity<String> response = this.restTemplate.getForEntity(URL + port + "product/6", String.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

}