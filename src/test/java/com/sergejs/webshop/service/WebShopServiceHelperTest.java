package com.sergejs.webshop.service;

import com.sergejs.webshop.dto.ProductListingDto;
import com.sergejs.webshop.exception.UnprocessableData;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.fail;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WebShopServiceHelperTest {

    private WebShopServiceHelper service = new WebShopServiceHelper();

    @Test(expected = UnprocessableData.class)
    public void stage01_validatorFirstError() {
        ProductListingDto product = new ProductListingDto();
        product.setPrice(-1);
        service.validateUpdateData(product);
    }

    @Test(expected = UnprocessableData.class)
    public void stage02_validatorSecondError() {
        ProductListingDto product = new ProductListingDto();
        product.setAmount(-1);
        service.validateUpdateData(product);
    }

    @Test
    public void stage03_okNUllName() {
        ProductListingDto product = new ProductListingDto();
        product.setAmount(12);
        product.setPrice(1234.11);
        try {
            service.validateUpdateData(product);
        } catch (UnprocessableData e) {
            fail("no UnprocesableData exception should be thrown");
        }
    }

    @Test
    public void stage04_okEmptyName() {
        ProductListingDto product = new ProductListingDto();
        product.setAmount(12);
        product.setPrice(1234.11);
        product.setName("");
        try {
            service.validateUpdateData(product);
        } catch (UnprocessableData e) {
            fail("no UnprocesableData exception should be thrown");
        }
    }

    @Test
    public void stage04_ok() {
        ProductListingDto product = new ProductListingDto();
        product.setAmount(12);
        product.setPrice(1234.11);
        product.setName("name");
        try {
            service.validateUpdateData(product);
        } catch (UnprocessableData e) {
            fail("no UnprocesableData exception should be thrown");
        }
    }


}