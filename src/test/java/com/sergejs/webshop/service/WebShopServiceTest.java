package com.sergejs.webshop.service;

import com.sergejs.webshop.dto.CollectionResponseDto;
import com.sergejs.webshop.dto.ProductListingDto;
import com.sergejs.webshop.dto.SingleResponseDto;
import com.sergejs.webshop.mapper.ProductListingMapper;
import com.sergejs.webshop.model.Product;
import com.sergejs.webshop.model.ProductRepository;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.*;

@RunWith(MockitoJUnitRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WebShopServiceTest {

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductListingMapper productListingMapper;

    @Mock
    private WebShopServiceHelper webShopServiceHelper;

    @InjectMocks
    private WebShopService service;

    @Test
    public void stage01_retrieveAllProducts() {
        Mockito.when(productRepository.findAll())
                .thenReturn(Arrays.asList(getTestProduct(1), getTestProduct(2)));
        Mockito.when(productListingMapper.toDto(any(Product.class)))
                .thenReturn(getTestProductDto(false));
        CollectionResponseDto<ProductListingDto> response = service.retrieveAllProducts();
        assertNotNull(response.getPayload());
        assertEquals(2, response.getPayload().size());
    }

    @Test
    public void stage02_retrieveProductsByType() {
        Mockito.when(productRepository.findByType(anyString()))
                .thenReturn(Arrays.asList(getTestProduct(1), getTestProduct(2), getTestProduct(3)));
        Mockito.when(productListingMapper.toDto(any(Product.class)))
                .thenReturn(getTestProductDto(false));
        CollectionResponseDto<ProductListingDto> response = service.retrieveProductsByType("laptop");
        assertNotNull(response.getPayload());
        assertEquals(3, response.getPayload().size());
    }

    @Test
    public void stage03_retrieveProductInfo() {
        Mockito.when(productRepository.findById(anyLong()))
                .thenReturn(getTestProduct(1L));
        Mockito.when(productListingMapper.toFullDto(any(Product.class)))
                .thenReturn(getTestProductDto(true));
        SingleResponseDto<ProductListingDto> response = service.getProductInfo(1L);
        assertNotNull(response);
        assertEquals(getTestProductDto(true), response.getPayload());
        Assert.assertTrue(response.getPayload().getDescription().length() >= 55);
    }

    @Test
    public void stage04_update() {
        Mockito.doNothing().when(webShopServiceHelper)
                .validateUpdateData(any(ProductListingDto.class));
        Mockito.when(productListingMapper.toEntity(any(ProductListingDto.class)))
                .thenReturn(getTestProduct(123L));
        Mockito.when(productRepository.save(any(Product.class)))
                .thenReturn(getTestProduct(123L));
        Mockito.when(productListingMapper.toFullDto(any(Product.class)))
                .thenReturn(getTestProductDto(true));

        ProductListingDto input = new ProductListingDto();
        input.setName("name");
        input.setDescription("description");
        input.setPrice(567);
        input.setAmount(456);

        SingleResponseDto<ProductListingDto> response = service.updateProduct(input);
        assertNotNull(response);
    }

    private Product getTestProduct(long id) {
        Product product = new Product();
        product.setProductId(id);
        product.setName("name");
        product.setDescription("description");
        product.setPrice(123);
        product.setAmount(321);
        return product;
    }

    private ProductListingDto getTestProductDto(boolean longDescription) {
        ProductListingDto product = new ProductListingDto();
        product.setName("name");
        if (!longDescription) {
            product.setDescription("description");
        } else {
            product.setDescription("descriptiondescriptiondescriptiondescript" +
                    "iondescriptiondescriptiondescriptiondescriptiondescriptiondescriptionde" +
                    "scriptiondescription");
        }
        product.setPrice(123);
        product.setAmount(321);
        return product;
    }

}