package com.sergejs.webshop;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WebShopApplicationTests {

    @Test
    public void contextLoads() {
        //	test checking context loading, asserting true because of sonarLint plugin complaining
        Assert.assertTrue(true);
    }

}
