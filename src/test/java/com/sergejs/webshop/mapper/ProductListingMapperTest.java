package com.sergejs.webshop.mapper;

import com.sergejs.webshop.dto.ProductListingDto;
import com.sergejs.webshop.model.Product;
import com.sergejs.webshop.utils.ProductType;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductListingMapperTest {

    private ProductListingMapper service = new ProductListingMapper();

    @Test
    public void stage01_toDto_empty() {
        Product entity = new Product();
        ProductListingDto dto = service.toDto(entity);
        assertNotNull(dto);
        assertEquals(0L, (long) dto.getId());
        assertNull(dto.getName());
        assertEquals(0L, dto.getAmount());
        assertNull(dto.getDescription());
    }

    @Test
    public void stage02_toDto_data() {
        Product entity = new Product();
        entity.setProductId(123L);
        entity.setAmount(1);
        entity.setPrice(45.4);
        entity.setDescription("pizza");
        entity.setName("name");
        entity.setType(ProductType.DESKTOP.toString());

        ProductListingDto dto = service.toDto(entity);
        assertNotNull(dto);
        assertEquals(123L, (long) dto.getId());
        assertEquals("name", dto.getName());
        assertEquals(1, dto.getAmount());
        assertEquals("pizza", dto.getDescription());
    }

    @Test
    public void stage03_testDescriptionshortener() {
        Product entity = new Product();
        entity.setDescription("descriptiondescriptiondescriptiondescriptiondescriptiondescription" +
                "descriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescription" +
                "descriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescription");
        ProductListingDto response = service.toDto(entity);
        assertNotNull(response);
        assertEquals(53, response.getDescription().length());
    }

    @Test
    public void stage04_toFullDto_empty() {
        Product entity = new Product();
        ProductListingDto dto = service.toFullDto(entity);
        assertNotNull(dto);
        assertEquals(0L, (long) dto.getId());
        assertNull(dto.getName());
        assertEquals(0L, dto.getAmount());
        assertNull(dto.getDescription());
    }

    @Test
    public void stage05_toFullDto_data() {
        Product entity = new Product();
        entity.setProductId(123L);
        entity.setAmount(1);
        entity.setPrice(45.4);
        entity.setDescription("pizza");
        entity.setName("name");
        entity.setType(ProductType.DESKTOP.toString());

        ProductListingDto dto = service.toFullDto(entity);
        assertNotNull(dto);
        assertEquals(123L, (long) dto.getId());
        assertEquals("name", dto.getName());
        assertEquals(1, dto.getAmount());
        assertEquals("pizza", dto.getDescription());
    }

    @Test
    public void stage06_toEntityEmpty() {
        ProductListingDto dto = new ProductListingDto();
        Product entity = service.toEntity(dto);
        assertNotNull(entity);
        assertNull(entity.getProductId());
        assertNull(entity.getDescription());
        assertEquals(0, entity.getAmount());
    }

    @Test
    public void stage07_toEntity() {
        ProductListingDto dto = new ProductListingDto();
        dto.setId(123L);
        dto.setAmount(1);
        dto.setPrice(45.4);
        dto.setDescription("pizza");
        dto.setName("name");

        Product entity = service.toEntity(dto);
        assertNotNull(entity);
        assertEquals(123L, (long) entity.getProductId());
        assertEquals("name", entity.getName());
        assertEquals(1, entity.getAmount());
        assertEquals("pizza", entity.getDescription());
    }

}